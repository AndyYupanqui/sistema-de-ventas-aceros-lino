'use strict'

var OrdenVenta = require('../models/ordenVenta');

const mysql = require('mysql');
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'inventariodb',
    port: 3306
 });
//var fs = require('fs');
//var path = require('path');

var controller = {

    nuevoOrdenVenta: function(req, res){
        var orden_venta = new OrdenVenta();
        var fecha_reg = new Date();
        var params = req.body;

        orden_venta.numero_venta = params.numero_venta;
        orden_venta.empleado = params.solicitante;
        orden_venta.cliente = params.cliente;
        orden_venta.descripcion = params.descripcion;
        orden_venta.fecha = fecha_reg.getDate() + "/" + (fecha_reg.getMonth() + 1) + "/" + fecha_reg.getFullYear();
        orden_venta.productos = params.producto;
        orden_venta.total = params.total;
        //orden_venta.image = null;
        var fecha = fecha_reg.getDate() + "/" + (fecha_reg.getMonth() + 1) + "/" + fecha_reg.getFullYear();
        var productos = JSON.stringify(params.producto);
        

        var query = connection.query('INSERT INTO ordenventas(productos, empleado, cliente, descripcion, fecha, total) VALUES(?, ?, ?, ?, ?, ?)', [productos, params.solicitante, params.cliente, params.descripcion, fecha, params.total], function(error, result){
            if(error){
               throw error;
            }else{
               console.log(result);
               console.log("se guardo");
               return res.status(200).send({ result });
            }
          }
        );

        /*
        return res.status(200).send({
            orden_venta: orden_venta,
            message: "Método saveOrdenVenta"
        });
        */
    },

    buscarOrdenVenta: function(req, res){
        var orden_ventaId = req.params.id;

        if(orden_ventaId == null) return res.status(404).send({message: 'La orden de venta no existe'});

        var query = connection.query('SELECT v._id, v.descripcion, c.dni FROM ordenventas v INNER JOIN clientes c ON c._id = v.cliente WHERE v._id=?', [orden_ventaId], function(error, result){
            if(error){
               throw error;
            }else{
               var orden_venta = result;
               if(orden_venta.length > 0){
                  //console.log(resultado[0].nombre + ' ' + resultado[0].apellido + ' / ' + resultado[0].biografia);
                  return res.status(200).send({ orden_venta });
               }else{
                  console.log('Registro no encontrado');
               }
            }
         }
        );
    },

    listarOrdenVentas: function(req, res){

        var query = connection.query('SELECT o._id, c.dni, c.nombres, c.apellidos, c.direccion, c.email, c.telefono, e.nombres as nombresEmp, e.apellidos as apellidosEmp, o.total, o.fecha FROM ordenventas o INNER JOIN clientes c ON c._id = o.cliente INNER JOIN empleados e ON e._id = o.empleado', [1], function(error, result){
            if(error){
               throw error;
            }else{
               var ordenes_ventas = result;
               if(ordenes_ventas.length >= 0){
                  return res.status(200).send({ ordenes_ventas });
               }else{
                  console.log('Registro no encontrado');
               }
            }
         }
        );

    },

    editarOrdenVenta: function(req, res){
        var orden_ventaId = req.params.id;
        var update = req.body;
    },

    borrarOrdenVenta: function(req, res){
        var orden_ventaId = req.params.id;

        var query = connection.query('DELETE FROM ordenventas WHERE _id=?', [orden_ventaId], function(error, result){
            if(error){
               throw error;
            }else{
                console.log('Registro eliminado');
                return res.status(200).send({ result });
            }
         }
        );
    }

    /*
    uploadImage: function(req, res){
        var orden_ventaId = req.params.id;
        var fileName = 'Imagen no subida...';

        if(req.files){
            var filePath = req.files.image.path;
            var fileSplit = filePath.split('\\');
            var fileName = fileSplit[1];
            var extSplit = fileName.split('\.');
            var fileExt = extSplit[1];

            if(fileExt == 'png' || fileExt =='jpg' || fileExt =='jpeg' || fileExt == 'gif'){
                
                OrdenVenta.findByIdAndUpdate(orden_ventaId, {image: fileName}, {new:true}, (err, orden_ventaUpdated) => {
                    if(err) return res.status(500).send({message: 'La imagen no se ha subido'});
    
                    if(!orden_ventaUpdated) return res.status(404).send({message: 'El orden_venta no existe y no se ha asignado la imagen'});
    
                    return res.status(200).send({
                        orden_venta: orden_ventaUpdated
                    });
                });

            }else{

                fs.unlink(filePath, (err) => {
                    return res.status(200).send({message: 'La extensión no es válida'});
                });

            }

        }else{
            return res.status(200).send({
                message: fileName
            })
        }
    },

    getImageFile: function(req, res){
        var file = req.params.image;
        var path_file = './uploads/'+file;

        fs.exists(path_file, (exists) => {
            if(exists){
                return res.sendFile(path.resolve(path_file))
            }else{
                return res.status(200).send({
                    message: "No existe la imagen..."
                });
            }
        });
    }
    */

};

module.exports = controller;