const controller = {};
const jwt = require('jsonwebtoken');
const Empleado = require('../models/empleado');
const bcrypt = require('bcrypt');

const mysql = require('mysql');
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'inventariodb',
    port: 3306
 });

controller.login = function(req, res) {

    let body = req.body;
    console.log(body);
    var correo = req.body.correo;

    // Empleado.findOne({
    //     email: body.correo
    // }
    var query = connection.query('SELECT * FROM empleados WHERE email=?', [correo], function(error, result){
        //console.log(result);
        var resp1 = result[0];
        var resp  = JSON.parse(JSON.stringify(resp1));
        if (error) {
            return res.status(500).json({
                ok: false,
                error
            })
        } else {
            if (!result) {
                return res.status(400).json({
                    ok: false,
                    error: {
                        message: '(Usuario) o contraseña incorrectos'
                    }
                })
            } else {
                if (!bcrypt.compareSync(body.password, resp.password)) {
                    return res.status(400).json({
                        ok: false,
                        error: {
                            message: 'Usuario o (contraseña) incorrectos'
                        }
                    })
                } else {
                    let token = jwt.sign({
                        resp
                    }, global.gConfig.jwt_code, { expiresIn: 60 * 60 });

                    return res.status(200).send({ token, resp });
                }
            }
        }
    })
}

controller.logout = async(req, res) => {

}

controller.getNombreReniec = async(req, res) => {

}

module.exports = controller;