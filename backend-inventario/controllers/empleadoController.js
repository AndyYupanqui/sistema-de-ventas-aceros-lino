'use strict'

const jwt = require('jsonwebtoken')
const Empleado = require('../models/empleado')
const bcrypt = require('bcrypt')
var fs = require('fs');
var path = require('path');

// var Storage = require('dom-storage');
// var localStorage = new Storage('./db.json', { strict: false, ws: '  ' });

const mysql = require('mysql');
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'inventariodb',
    port: 3306
 });

 var controller = {

    nuevoEmpleado: function(request, response) {

    let body = response.req.body;
    console.log(body)

    let empleado = new Empleado({
        dni: body.dni,
        nombres: body.nombres,
        apellidos: body.apellidos,
        email: body.email,
        password: bcrypt.hashSync(body.password, 10),
        edad: body.edad,
        direccion: body.direccion,
        telefono: body.telefono,
        celular: body.celular,
        role: body.role,
        img: body.img,
        estado: body.estado
    })

    // await empleado.save((err, empleadoDB) => {
    //     if (err) {
    //         response.status(400).json({
    //             ok: false,
    //             err
    //         });
    //     } else {
    //         empleadoDB.password = "";
    //         console.log(empleadoDB);
    //         response.json({
    //             ok: true,
    //             empleado: empleadoDB
    //         })
    //     }
    // });

    var query = connection.query('INSERT INTO empleados(role, img, dni, nombres, apellidos, email, password, edad, direccion) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)', [body.role, body.img, body.dni, body.nombres, body.apellidos, body.email, bcrypt.hashSync(body.password, 10), body.edad, body.direccion], function(error, result){
        if(error){
           throw error;
        }else{
            var q = connection.query('SELECT * FROM empleados WHERE dni=?', [body.dni], function(err, resp){
                console.log("se guardo");
                return response.status(200).send({ resp });
            });
        }
        //connection.end();
      }
     );
    },

    buscarEmpleado: function(req, res) {
        var empleadoId = req.params.id;

        if (empleadoId == null) return res.status(404).send({ message: 'El empleado no existe' });

        // await Empleado.findById(empleadoId, (err, empleado) => {

        //     if (err) {
        //         if (!empleado) return res.status(404).send({ message: 'El empleado no existe.' });
        //         return res.status(500).send({ message: 'Error al devolver los datos.' });
        //     }

        //     return res.status(200).send({
        //         empleado
        //     });

        // });

        var query = connection.query('SELECT * FROM empleados WHERE _id=?', [empleadoId], function(error, result){
            if(error){
            throw error;
            }else{
            var empleado = result;
            if(empleado.length > 0){
                //console.log(resultado[0].nombre + ' ' + resultado[0].apellido + ' / ' + resultado[0].biografia);
                return res.status(200).send({ empleado });
            }else{
                console.log('Registro no encontrado');
            }
            //connection.end();
            }
        }
        );
    },

    buscarDniEmpleado: function(req, res) {
        var empleadoDni = req.params.dni;

        if (empleadoDni == null) return res.status(404).send({ message: 'El empleado no existe' });

        // await Empleado.findOne({ dni: empleadoDni }, (err, empleado) => {

        //     if (err) {
        //         if (!empleado) return res.status(404).send({ message: 'El empleado no existe.' });
        //         return res.status(500).send({ message: 'Error al devolver los datos.' });
        //     }

        //     return res.status(200).send({
        //         empleado
        //     });

        // });

        var query = connection.query('SELECT * FROM empleados WHERE dni=?', [empleadoDni], function(error, result){
            if(error){
            throw error;
            }else{
            var empleado = result;
            if(empleado.length > 0){
                //console.log(resultado[0].nombre + ' ' + resultado[0].apellido + ' / ' + resultado[0].biografia);
                return res.status(200).send({ empleado });
            }else{
                console.log('Registro no encontrado');
            }
            //connection.end();
            }
        }
        ); 
    },

    listarEmpleados: function(request, response) {
        // await Empleado.find({}).exec((err, listaUsuarios) => {
        //     if (err) {
        //         return response.status(400).json({
        //             ok: false,
        //             err
        //         })
        //     } else {
        //         response.json({
        //             ok: true,
        //             listaUsuarios
        //         })
        //     }
        // })
        
        var query = connection.query('SELECT * FROM empleados', [1], function(error, result){
            //console.log(result);
            if(error){
                throw error;
            }else{
            var empleados = JSON.parse(JSON.stringify(result));
            if(empleados.length >= 0){
                //console.log(empleados);
                return response.status(200).send({ empleados });
            }else{
                console.log('Registro no encontrado');
            }
            }
            //connection.end();
        }
        );

    },  

    editarEmpleado: function(req, res) {
        let id = req.params.id;
        console.log(req.params);
        let body = req.body;
        console.log(body);
        // await Empleado.findByIdAndUpdate(id, body, { new: true }, (err, usuarioDB) => {
        //     if (err) {
        //         return response.status(400).json({
        //             ok: false,
        //             err
        //         })
        //     } else {
        //         response.json({
        //             ok: true,
        //             id
        //         });
        //     }

        // });

        var query = connection.query('UPDATE empleados SET direccion=?, email=?, nombres=?, apellidos=?, dni=?, edad=?, role=? WHERE _id=?', [body.direccion, body.email, body.nombres, body.apellidos, body.dni, body.edad, body.role, id], function(error, result){
            if(error){
            throw error;
            }else{
            console.log(result);
            console.log("se actualizo");
            return res.status(200).send({ result });
            }
        }
        );
},

    borrarEmpleado: function(request, response) {

        let id = request.params.id;
        let body = request.body;

        // await Empleado.findByIdAndRemove(id, (err, usuarioDB) => {
        //     if (err) {
        //         return response.status(400).json({
        //             ok: false,
        //             err
        //         })
        //     } else {
        //         response.json({
        //             ok: true,
        //             id
        //         });
        //     }

        // });

        var query = connection.query('DELETE FROM empleados WHERE _id=?', [id], function(error, result){
            if(error){
            throw error;
            }else{
                console.log('Registro eliminado');
                return response.status(200).send({ result });
            }
            //connection.end();
        }
        );
    },

    uploadImage: function(req, res) {
        var empleadoId = req.params.id;
        var fileName = 'Imagen no subida...';
        var resp;
        if (req.files) {
            //console.log(req.files);
            var filePath = req.files.img.path;
            var fileSplit = filePath.split('\\');
            var fileName = fileSplit[1];
            var extSplit = fileName.split('\.');
            var fileExt = extSplit[1].toLowerCase();

            if (fileExt == 'png' || fileExt == 'jpg' || fileExt == 'jpeg' || fileExt == 'gif') {

                // Empleado.findByIdAndUpdate(empleadoId, { img: fileName }, { new: true }, (err, empleadoUpdated) => {

                //     if (err) {
                //         if (!empleadoUpdated) return res.status(404).send({ message: 'El empleado no existe y no se ha asignado la imagen' });
                //         return res.status(500).send({ message: 'La imagen no se ha subido' });
                //     } else {
                //         return res.status(200).send({
                //             empleado: empleadoUpdated
                //         });
                //     }
                // });
                console.log(fileName);

                var query = connection.query('UPDATE empleados SET img=? WHERE _id=?', [fileName, empleadoId], function(error, result){
                    if(error){
                        throw error;
                    }else{
                        var q = connection.query('SELECT * FROM empleados WHERE _id=?', [empleadoId], function(err, resp){
                            // localStorage.setItem('imagen', JSON.stringify(r[0]));
                            // resp = JSON.parse(localStorage.getItem('imagen'));
                            console.log("se cambio imagen");
                            return res.status(200).send({ resp });
                        });

                    }
                    //connection.end();
                }
                );

            } else {
                fs.unlink(filePath, (err) => {
                    return res.status(200).send({ message: 'La extensión no es válida' });
                });
            }
        } else {
            return res.status(200).send({
                message: fileName
            })
        }
    },

    getImageFile: function (req, res) {
        var file = req.params.image;
        var path_file = './uploads/' + file;

        fs.exists(path_file, (exists) => {
            if (exists) {
                return res.sendFile(path.resolve(path_file))
            } else {
                return res.status(200).send({
                    message: "No existe la imagen..."
                });
            }
        });
    }
 }

module.exports = controller;