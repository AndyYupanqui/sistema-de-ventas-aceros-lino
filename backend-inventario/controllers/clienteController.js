'use strict'

var Cliente = require('../models/cliente');

const mysql = require('mysql');
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'inventariodb',
    port: 3306
 });
//var fs = require('fs');
//var path = require('path');

var controller = {

    nuevoCliente: function(req, res) {
        var cliente = new Cliente();

        var params = req.body;
        cliente.dni = params.dni;
        cliente.nombres = params.nombres;
        cliente.apellidos = params.apellidos;
        cliente.direccion = params.direccion;
        cliente.telefono = params.telefono;
        cliente.celular = params.celular;
        cliente.email = params.email;
        //cliente.image = null;

        var query = connection.query('INSERT INTO clientes(dni, nombres, apellidos, direccion, telefono, celular, email) VALUES(?, ?, ?, ?, ?, ?, ?)', [params.dni, params.nombres, params.apellidos, params.direccion, params.telefono, params.celular, params.email], function(error, result){
            if(error){
               throw error;
            }else{
               console.log(result);
               console.log("se guardo");
               return res.status(200).send({ result });
            }
          }
        );

        /*
        return res.status(200).send({
            cliente: cliente,
            message: "Método saveCliente"
        });
        */
    },

    buscarCliente: function(req, res) {
        var clienteId = req.params.id;

        if (clienteId == null) return res.status(404).send({ message: 'El cliente no existe' });

        var query = connection.query('SELECT * FROM clientes WHERE _id=?', [clienteId], function(error, result){
            if(error){
               throw error;
            }else{
               var cliente = result;
               if(cliente.length > 0){
                  //console.log(resultado[0].nombre + ' ' + resultado[0].apellido + ' / ' + resultado[0].biografia);
                  return res.status(200).send({ cliente });
               }else{
                  console.log('Registro no encontrado');
               }
            }
         }
        );
    },

    buscarDniCliente: function(req, res) {
        var clienteDni = req.params.dni;

        if (clienteDni == null) return res.status(404).send({ message: 'El cliente no existe' });

        var query = connection.query('SELECT * FROM clientes WHERE dni=?', [clienteDni], function(error, result){
            if(error){
               throw error;
            }else{
               var cliente = result;
               if(cliente.length > 0){
                  //console.log(resultado[0].nombre + ' ' + resultado[0].apellido + ' / ' + resultado[0].biografia);
                  return res.status(200).send({ cliente });
               }else{
                  console.log('Registro no encontrado');
               }
            }
         }
        );
    },

    listarClientes: function(req, res) {

        var query = connection.query('SELECT * FROM clientes', [1], function(error, result){
            if(error){
               throw error;
            }else{
               var clientes = result;
               if(clientes.length >= 0){
                  return res.status(200).send({ clientes });
               }else{
                  console.log('Registro no encontrado');
               }
            }
         }
        );

    },

    editarCliente: function(req, res) {
        var clienteId = req.params.id;
        var update = req.body;

        var query = connection.query('UPDATE clientes SET nombres=?, apellidos=?, direccion=?, telefono=?, celular=?, email=? WHERE _id=?', [update.nombres, update.apellidos, update.direccion, update.telefono, update.celular, update.email, clienteId], function(error, result){
            if(error){
               throw error;
            }else{
               console.log("se actualizo");
               return res.status(200).send({ result });
            }
          }
         );

    },

    borrarCliente: function(req, res) {
        var clienteId = req.params.id;

        var query = connection.query('DELETE FROM clientes WHERE _id=?', [clienteId], function(error, result){
            if(error){
               throw error;
            }else{
                console.log('Registro eliminado');
                return res.status(200).send({ result });
            }
         }
        );
    }

    /*
    uploadImage: function(req, res){
        var clienteId = req.params.id;
        var fileName = 'Imagen no subida...';

        if(req.files){
            var filePath = req.files.image.path;
            var fileSplit = filePath.split('\\');
            var fileName = fileSplit[1];
            var extSplit = fileName.split('\.');
            var fileExt = extSplit[1];

            if(fileExt == 'png' || fileExt =='jpg' || fileExt =='jpeg' || fileExt == 'gif'){
                
                Cliente.findByIdAndUpdate(clienteId, {image: fileName}, {new:true}, (err, clienteUpdated) => {
                    if(err) return res.status(500).send({message: 'La imagen no se ha subido'});
    
                    if(!clienteUpdated) return res.status(404).send({message: 'El cliente no existe y no se ha asignado la imagen'});
    
                    return res.status(200).send({
                        cliente: clienteUpdated
                    });
                });

            }else{

                fs.unlink(filePath, (err) => {
                    return res.status(200).send({message: 'La extensión no es válida'});
                });

            }

        }else{
            return res.status(200).send({
                message: fileName
            })
        }
    },

    getImageFile: function(req, res){
        var file = req.params.image;
        var path_file = './uploads/'+file;

        fs.exists(path_file, (exists) => {
            if(exists){
                return res.sendFile(path.resolve(path_file))
            }else{
                return res.status(200).send({
                    message: "No existe la imagen..."
                });
            }
        });
    }
    */

};

module.exports = controller;