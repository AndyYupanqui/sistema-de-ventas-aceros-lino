'use strict'

var Proveedor = require('../models/proveedor');

const mysql = require('mysql');
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'inventariodb',
    port: 3306
 });
//var fs = require('fs');
//var path = require('path');

var controller = {

    nuevoProveedor: function(req, res) {
        var proveedor = new Proveedor();

        var params = req.body;
        proveedor.ruc = params.ruc;
        proveedor.nombre = params.nombre;
        proveedor.direccion = params.direccion;
        proveedor.telefono = params.telefono;
        proveedor.celular = params.celular;
        proveedor.email = params.email;
        //proveedor.image = null;

        // proveedor.save((err, proveedorStored) => {

        //     if (err) {
        //         if (!proveedorStored) return res.status(404).send({ message: 'No se ha podido guardar el proveedor.' });
        //         return res.status(500).send({ message: 'Error al guardar el documento.' });
        //     }

        //     return res.status(200).send({ proveedor: proveedorStored });
        // });

        var query = connection.query('INSERT INTO proveedors(ruc, nombre, direccion, telefono, celular, email) VALUES(?, ?, ?, ?, ?, ?)', [params.ruc, params.nombre, params.direccion, params.telefono, params.celular, params.email], function(error, result){
            if(error){
               throw error;
            }else{
               console.log(result);
               console.log("se guardo");
               return res.status(200).send({ result });
            }
          }
        );

        /*
        return res.status(200).send({
            proveedor: proveedor,
            message: "Método saveProveedor"
        });
        */
    },

    buscarProveedor: function(req, res) {
        var proveedorId = req.params.id;

        if (proveedorId == null) return res.status(404).send({ message: 'El proveedor no existe' });

        var query = connection.query('SELECT * FROM proveedors WHERE _id=?', [proveedorId], function(error, result){
            if(error){
               throw error;
            }else{
               var proveedor = result;
               if(proveedor.length > 0){
                  //console.log(resultado[0].nombre + ' ' + resultado[0].apellido + ' / ' + resultado[0].biografia);
                  return res.status(200).send({ proveedor });
               }else{
                  console.log('Registro no encontrado');
               }
            }
         }
        );
    },

    buscarRucProveedor: function(req, res) {
        var proveedorRuc = req.params.ruc;

        if (proveedorRuc == null) return res.status(404).send({ message: 'El proveedor no existe' });

        var query = connection.query('SELECT * FROM proveedors WHERE ruc=?', [proveedorRuc], function(error, result){
            if(error){
               throw error;
            }else{
               var proveedor = result;
               if(proveedor.length > 0){
                  //console.log(resultado[0].nombre + ' ' + resultado[0].apellido + ' / ' + resultado[0].biografia);
                  return res.status(200).send({ proveedor });
               }else{
                  console.log('Registro no encontrado');
               }
            }
         }
        );
    },

    listarProveedores: function(req, res) {

        var query = connection.query('SELECT * FROM proveedors', [1], function(error, result){
            if(error){
               throw error;
            }else{
               var proveedores = result;
               //console.log(proveedores);
               if(proveedores.length >= 0){
                  return res.status(200).send({ proveedores });
               }else{
                  console.log('Registro no encontrado');
               }
            }
         }
        );

    },

    editarProveedor: function(req, res) {
        var proveedorId = req.params.id;
        var update = req.body;

        var query = connection.query('UPDATE proveedors SET nombre=?, direccion=?, telefono=?, ruc=?, celular=?, email=? WHERE _id=?', [update.nombre, update.direccion, update.telefono, update.ruc, update.celular, update.email, proveedorId], function(error, result){
            if(error){
               throw error;
            }else{
               console.log("se actualizo");
               return res.status(200).send({ result });
            }
          }
         );

    },

    borrarProveedor: function(req, res) {
        var proveedorId = req.params.id;

        var query = connection.query('DELETE FROM proveedors WHERE _id=?', [proveedorId], function(error, result){
            if(error){
               throw error;
            }else{
                console.log('Registro eliminado');
                return res.status(200).send({ result });
            }
         }
        );
    }

    /*
    uploadImage: function(req, res){
        var proveedorId = req.params.id;
        var fileName = 'Imagen no subida...';

        if(req.files){
            var filePath = req.files.image.path;
            var fileSplit = filePath.split('\\');
            var fileName = fileSplit[1];
            var extSplit = fileName.split('\.');
            var fileExt = extSplit[1];

            if(fileExt == 'png' || fileExt =='jpg' || fileExt =='jpeg' || fileExt == 'gif'){
                
                Proveedor.findByIdAndUpdate(proveedorId, {image: fileName}, {new:true}, (err, proveedorUpdated) => {
                    if(err) return res.status(500).send({message: 'La imagen no se ha subido'});
    
                    if(!proveedorUpdated) return res.status(404).send({message: 'El proveedor no existe y no se ha asignado la imagen'});
    
                    return res.status(200).send({
                        proveedor: proveedorUpdated
                    });
                });

            }else{

                fs.unlink(filePath, (err) => {
                    return res.status(200).send({message: 'La extensión no es válida'});
                });

            }

        }else{
            return res.status(200).send({
                message: fileName
            })
        }
    },

    getImageFile: function(req, res){
        var file = req.params.image;
        var path_file = './uploads/'+file;

        fs.exists(path_file, (exists) => {
            if(exists){
                return res.sendFile(path.resolve(path_file))
            }else{
                return res.status(200).send({
                    message: "No existe la imagen..."
                });
            }
        });
    }
    */

};

module.exports = controller;