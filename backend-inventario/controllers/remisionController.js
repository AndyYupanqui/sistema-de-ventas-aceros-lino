'use strict'

var Remision = require('../models/remision');

const mysql = require('mysql');
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'inventariodb',
    port: 3306
 });
//var fs = require('fs');
//var path = require('path');

var controller = {

    nuevoRemision: function(req, res){
        var remision = new Remision();
        var params = req.body;
        
        remision.numero_remision = params.numero_remision;
        remision.fechaInicio = params.fechaIni;
        remision.partida = params.puntoPartida;
        remision.ruc = params.ruc;
        remision.motivo = params.motivo;
        remision.rucTrans = params.rucTrans;
        remision.denominacion = params.denominacion;
        remision.marcaPlaca = params.marcaPlaca;
        remision.licencia = params.licencia;
        remision.cantidad = params.cantidad;

        var cantidad = JSON.stringify(params.cantidad);

        var query = connection.query('INSERT INTO remisions(cantidad, numero_remision, fechaInicio, partida, ruc, motivo, rucTrans, denominacion, marcaPlaca, licencia) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [cantidad, params.numero_remision, params.fechaIni, params.puntoPartida, params.ruc, params.motivo, params.rucTrans, params.denominacion, params.marcaPlaca, params.licencia], function(error, result){
            if(error){
               throw error;
            }else{
               console.log(result);
               console.log("se guardo");
               return res.status(200).send({ result });
            }
          }
        );

        /*
        return res.status(200).send({
            orden_compra: orden_compra,
            message: "Método saveOrdenCompra"
        });
        */
    },

    buscarRemision: function(req, res){
        var remisionId = req.params.id;

        if(remisionId == null) return res.status(404).send({message: 'La guía de remisión no existe'});

        var query = connection.query('SELECT * FROM remisions WHERE numero_remision=?', [remisionId], function(error, result){
            if(error){
               throw error;
            }else{
               var remision = result;
               if(remision.length >= 0){
                  return res.status(200).send({ remision });
               }else{
                  console.log('Registro no encontrado');
               }
            }
         }
        );
    }

    // listarRemision: function(req, res){

    //     OrdenCompra.find({}).populate('empleado').populate('proveedor').populate('productos').exec((err, ordenes_compras) => {

    //         if(err){
    //             if(!ordenes_compras) return res.status(404).send({message: 'No hay ordenes de compras que mostrar.'});
    //             return res.status(500).send({message: 'Error al devolver los datos.'});
    //         } 

    //         return res.status(200).send({ordenes_compras});
    //     });

    // },

    // editarOrdenCompra: function(req, res){
    //     var orden_compraId = req.params.id;
    //     var update = req.body;

    //     OrdenCompra.findByIdAndUpdate(orden_compraId, update, {new:true}, (err, orden_compraUpdated) => {
            
    //         if(err){
    //             if(!orden_compraUpdated) return res.status(404).send({message: 'No existe la orden de compra para actualizar'});
    //             return res.status(500).send({message: 'Error al actualizar'});
    //         } 

    //         return res.status(200).send({
    //             orden_compra: orden_compraUpdated 
    //         });

    //     });

    // },

    // borrarOrdenCompra: function(req, res){
    //     var orden_compraId = req.params.id;

    //     OrdenCompra.findByIdAndRemove(orden_compraId, (err, orden_compraRemoved) => {

    //         if(err){
    //             if(!orden_compraRemoved) return res.status(404).send({message: "No se puede eliminar esa orden de compra porque no se encuentra."});
    //             return res.status(500).send({message: 'No se ha podido borrar la orden de compra'});
    //         } 

    //         return res.status(200).send({
    //             orden_compra: orden_compraRemoved
    //         });
    //     });
    // }

    /*
    uploadImage: function(req, res){
        var orden_compraId = req.params.id;
        var fileName = 'Imagen no subida...';

        if(req.files){
            var filePath = req.files.image.path;
            var fileSplit = filePath.split('\\');
            var fileName = fileSplit[1];
            var extSplit = fileName.split('\.');
            var fileExt = extSplit[1];

            if(fileExt == 'png' || fileExt =='jpg' || fileExt =='jpeg' || fileExt == 'gif'){
                
                OrdenCompra.findByIdAndUpdate(orden_compraId, {image: fileName}, {new:true}, (err, orden_compraUpdated) => {
                    if(err) return res.status(500).send({message: 'La imagen no se ha subido'});
    
                    if(!orden_compraUpdated) return res.status(404).send({message: 'El orden_compra no existe y no se ha asignado la imagen'});
    
                    return res.status(200).send({
                        orden_compra: orden_compraUpdated
                    });
                });

            }else{

                fs.unlink(filePath, (err) => {
                    return res.status(200).send({message: 'La extensión no es válida'});
                });

            }

        }else{
            return res.status(200).send({
                message: fileName
            })
        }
    },

    getImageFile: function(req, res){
        var file = req.params.image;
        var path_file = './uploads/'+file;

        fs.exists(path_file, (exists) => {
            if(exists){
                return res.sendFile(path.resolve(path_file))
            }else{
                return res.status(200).send({
                    message: "No existe la imagen..."
                });
            }
        });
    }
    */

};

module.exports = controller;