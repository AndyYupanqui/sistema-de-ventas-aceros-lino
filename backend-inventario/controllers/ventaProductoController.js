'use strict'

var VentaProducto = require('../models/ventaProducto');

const mysql = require('mysql');
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'inventariodb',
    port: 3306
 });
//var fs = require('fs');
//var path = require('path');

var controller = {

    nuevoVentaProducto: function(req, res) {
        var venta_producto = new VentaProducto();
        var params = req.body;

        venta_producto.venta = params.venta;
        venta_producto.producto = params.producto;
        venta_producto.cantidad = params.cantidad;
        venta_producto.estado = params.estado;

        console.log(params);

        var query = connection.query('INSERT INTO ventaproductos(estado, venta, producto, cantidad) VALUES(?, ?, ?, ?)', [params.estado, params.venta, params.producto, params.cantidad], function(error, result){
            if(error){
               throw error;
            }else{
               console.log(result);
               console.log("se guardo");
               return res.status(200).send({ result });
            }
          }
        );

        /*
        return res.status(200).send({
            orden_compra: compra_producto,
            message: "Método saveCompraProducto"
        });
        */
    },

    buscarVentaProducto: function(req, res) {
        var venta_productoVenta = req.params.venta;

        if (venta_productoVenta == null) return res.status(404).send({ message: 'La venta del producto no existe' });

        var query = connection.query('SELECT v._id, v.estado, v.venta, v.producto, v.cantidad, p.nombre as nombreProd, p._id as idProd, p.descripcion, p.stock, p.precio FROM ventaproductos v INNER JOIN productos p ON p._id = v.producto WHERE v.venta=?', [venta_productoVenta], function(error, result){
            if(error){
               throw error;
            }else{
               var venta_producto = result;
               if(venta_producto.length > 0){
                  //console.log(resultado[0].nombre + ' ' + resultado[0].apellido + ' / ' + resultado[0].biografia);
                  return res.status(200).send({ venta_producto });
               }else{
                  console.log('Registro no encontrado');
               }
            }
         });
    },

    // buscarCompraProducto: function(req, res){
    //     var compra_productoCompra = req.params.compra;
    //     var compra_productoProducto = req.params.producto;

    //     if(compra_productoCompra == null || compra_productoProducto == null) return res.status(404).send({message: 'La compra del producto no existe'});

    //     CompraProducto.findOne({ compra: compra_productoCompra, producto: compra_productoProducto }, (err, compra_producto) => {

    //         if(!compra_producto) return res.status(404).send({message: 'La compra del producto no existe.'});

    //         if(err) {
    //             return res.status(500).send({message: 'Error al devolver los datos.'});
    //         }

    //         return res.status(200).send({
    //             compra_producto
    //         });

    //     }).populate('compra').populate('producto');
    // },

    listarVentaProductos: function(req, res) {

        var query = connection.query('SELECT v._id, v.estado, v.venta, v.producto, v.cantidad, p.nombre as nombreProd, p._id as idProd, p.descripcion, p.stock, p.precio, vn.descripcion, vn.fecha, vn.cliente, cl.dni FROM ventaproductos v INNER JOIN productos p ON p._id = v.producto INNER JOIN ordenventas vn ON vn._id = v.venta INNER JOIN clientes cl ON cl._id = vn.cliente', [1], function(error, result){
            if(error){
               throw error;
            }else{
               var ventas_productos = result;
               if(ventas_productos.length >= 0){
                  return res.status(200).send({ ventas_productos });
               }else{
                  console.log('Registro no encontrado');
               }
            }
         }
        );

    },

    editarVentaProducto: function(req, res) {
        var venta_productoId = req.params.id;
        var update = req.body;

        var query = connection.query('UPDATE ventaproductos SET venta=?, producto=?, cantidad=?, estado=? WHERE _id=?', [update.venta, update.producto, update.cantidad, update.estado, venta_productoId], function(error, result){
            if(error){
               throw error;
            }else{
               console.log("se actualizo");
               return res.status(200).send({ result });
            }
          }
         );

    },

    borrarVentaProducto: function(req, res) {
        var venta_productoId = req.params.id;

        var query = connection.query('DELETE FROM ventaproductos WHERE _id=?', [venta_productoId], function(error, result){
            if(error){
               throw error;
            }else{
                console.log('Registro eliminado');
                return res.status(200).send({ result });
            }
         }
        );
    }
};

module.exports = controller;