'use strict'

var OrdenCompra = require('../models/ordenCompra');

const mysql = require('mysql');
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'inventariodb',
    port: 3306
 });
//var fs = require('fs');
//var path = require('path');

var controller = {

    nuevoOrdenCompra: function(req, res){
        var orden_compra = new OrdenCompra();
        var fecha_reg = new Date();
        var params = req.body;
        
        orden_compra.numero_compra = params.numero_compra;
        orden_compra.empleado = params.solicitante;
        orden_compra.proveedor = params.proveedor;
        orden_compra.descripcion = params.descripcion;
        orden_compra.fecha = fecha_reg.getDate() + "/" + (fecha_reg.getMonth() + 1) + "/" + fecha_reg.getFullYear();
        orden_compra.productos = params.producto;
        orden_compra.total = params.total;
        //orden_compra.image = null;
        var fecha = fecha_reg.getDate() + "/" + (fecha_reg.getMonth() + 1) + "/" + fecha_reg.getFullYear();
        var productos = JSON.stringify(params.producto);

        var query = connection.query('INSERT INTO ordencompras(productos, empleado, proveedor, descripcion, fecha, total) VALUES(?, ?, ?, ?, ?, ?)', [productos, params.solicitante, params.proveedor, params.descripcion, fecha, params.total], function(error, result){
            if(error){
               throw error;
            }else{
               console.log(result);
               console.log("se guardo");
               return res.status(200).send({ result });
            }
          }
        );

        /*
        return res.status(200).send({
            orden_compra: orden_compra,
            message: "Método saveOrdenCompra"
        });
        */
    },

    buscarOrdenCompra: function(req, res){
        var orden_compraId = req.params.id;

        if(orden_compraId == null) return res.status(404).send({message: 'La orden de compra no existe'});

        var query = connection.query('SELECT * FROM ordencompras WHERE _id=?', [orden_compraId], function(error, result){
            if(error){
               throw error;
            }else{
               var orden_compra = result;
               if(orden_compra.length > 0){
                  //console.log(resultado[0].nombre + ' ' + resultado[0].apellido + ' / ' + resultado[0].biografia);
                  return res.status(200).send({ orden_compra });
               }else{
                  console.log('Registro no encontrado');
               }
            }
         }
        );
    },

    listarOrdenCompras: function(req, res){

        var query = connection.query('SELECT o._id, p.nombre, e.nombres, e.apellidos, o.total, o.fecha, p.direccion, p.ruc FROM ordencompras o INNER JOIN proveedors p ON p._id = o.proveedor INNER JOIN empleados e ON e._id = o.empleado', [1], function(error, result){
            if(error){
               throw error;
            }else{
               var ordenes_compras = result;
               if(ordenes_compras.length >= 0){
                  return res.status(200).send({ ordenes_compras });
               }else{
                  console.log('Registro no encontrado');
               }
            }
         }
        );

    },

    editarOrdenCompra: function(req, res){
        var orden_compraId = req.params.id;
        var update = req.body;

    },

    borrarOrdenCompra: function(req, res){
        var orden_compraId = req.params.id;

        var query = connection.query('DELETE FROM ordencompras WHERE _id=?', [orden_compraId], function(error, result){
            if(error){
               throw error;
            }else{
                console.log('Registro eliminado');
                return res.status(200).send({ result });
            }
         }
        );
    }

    /*
    uploadImage: function(req, res){
        var orden_compraId = req.params.id;
        var fileName = 'Imagen no subida...';

        if(req.files){
            var filePath = req.files.image.path;
            var fileSplit = filePath.split('\\');
            var fileName = fileSplit[1];
            var extSplit = fileName.split('\.');
            var fileExt = extSplit[1];

            if(fileExt == 'png' || fileExt =='jpg' || fileExt =='jpeg' || fileExt == 'gif'){
                
                OrdenCompra.findByIdAndUpdate(orden_compraId, {image: fileName}, {new:true}, (err, orden_compraUpdated) => {
                    if(err) return res.status(500).send({message: 'La imagen no se ha subido'});
    
                    if(!orden_compraUpdated) return res.status(404).send({message: 'El orden_compra no existe y no se ha asignado la imagen'});
    
                    return res.status(200).send({
                        orden_compra: orden_compraUpdated
                    });
                });

            }else{

                fs.unlink(filePath, (err) => {
                    return res.status(200).send({message: 'La extensión no es válida'});
                });

            }

        }else{
            return res.status(200).send({
                message: fileName
            })
        }
    },

    getImageFile: function(req, res){
        var file = req.params.image;
        var path_file = './uploads/'+file;

        fs.exists(path_file, (exists) => {
            if(exists){
                return res.sendFile(path.resolve(path_file))
            }else{
                return res.status(200).send({
                    message: "No existe la imagen..."
                });
            }
        });
    }
    */

};

module.exports = controller;