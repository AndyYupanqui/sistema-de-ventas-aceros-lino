'use strict'

var CompraProducto = require('../models/compraProducto');

const mysql = require('mysql');
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'inventariodb',
    port: 3306
 });
//var fs = require('fs');
//var path = require('path');

var controller = {

    nuevoCompraProducto: function(req, res){
        var compra_producto = new CompraProducto();
        var params = req.body;
        
        compra_producto.compra = params.compra;
        compra_producto.producto = params.producto;
        compra_producto.cantidad = params.cantidad;
        compra_producto.estado = params.estado;

        // compra_producto.save((err, compra_productoStored) => {

        //     if(err) {
        //         if(!compra_productoStored) return res.status(404).send({message: 'No se ha podido guardar la compra del producto.'});
        //         return res.status(500).send({message: 'Error al guardar el documento.'});
        //     }

        //     return res.status(200).send({compra_producto: compra_productoStored});
        // });

        var query = connection.query('INSERT INTO compraproductos(estado, compra, producto, cantidad) VALUES(?, ?, ?, ?)', [params.estado, params.compra, params.producto, params.cantidad], function(error, result){
            if(error){
               throw error;
            }else{
               console.log(result);
               console.log("se guardo");
               return res.status(200).send({ result });
            }
          }
        );

        /*
        return res.status(200).send({
            orden_compra: compra_producto,
            message: "Método saveCompraProducto"
        });
        */
    },

    buscarCompraProducto: function(req, res){
        var compra_productoCompra = req.params.compra;

        if(compra_productoCompra == null) return res.status(404).send({message: 'La compra del producto no existe'});

        // CompraProducto.find({ compra: compra_productoCompra }, (err, compra_producto) => {
            
        //     if(!compra_producto) return res.status(404).send({message: 'La compra del producto no existe.'});

        //     if(err) {
        //         return res.status(500).send({message: 'Error al devolver los datos.'});
        //     }

        //     return res.status(200).send({
        //         compra_producto
        //     });

        // }).populate('compra').populate('producto');

        var query = connection.query('SELECT c._id, c.estado, c.compra, c.producto, c.cantidad, p.nombre as nombreProd, p._id as idProd, p.descripcion, p.stock, p.precio FROM compraproductos c INNER JOIN productos p ON p._id = c.producto WHERE c.compra=?', [compra_productoCompra], function(error, result){
            if(error){
               throw error;
            }else{
               var compra_producto = result;
               if(compra_producto.length > 0){
                  //console.log(resultado[0].nombre + ' ' + resultado[0].apellido + ' / ' + resultado[0].biografia);
                  return res.status(200).send({ compra_producto });
               }else{
                  console.log('Registro no encontrado');
               }
            }
         });
    },

    // buscarCompraProducto: function(req, res){
    //     var compra_productoCompra = req.params.compra;
    //     var compra_productoProducto = req.params.producto;

    //     if(compra_productoCompra == null || compra_productoProducto == null) return res.status(404).send({message: 'La compra del producto no existe'});

    //     CompraProducto.findOne({ compra: compra_productoCompra, producto: compra_productoProducto }, (err, compra_producto) => {
            
    //         if(!compra_producto) return res.status(404).send({message: 'La compra del producto no existe.'});

    //         if(err) {
    //             return res.status(500).send({message: 'Error al devolver los datos.'});
    //         }

    //         return res.status(200).send({
    //             compra_producto
    //         });

    //     }).populate('compra').populate('producto');
    // },

    listarCompraProductos: function(req, res){

        // CompraProducto.find({}).populate('compra').populate('producto').exec((err, compras_productos) => {

        //     if(err){
        //         if(!compras_productos) return res.status(404).send({message: 'No hay compras de productos que mostrar.'});
        //         return res.status(500).send({message: 'Error al devolver los datos.'});
        //     } 

        //     return res.status(200).send({compras_productos});
        // });

        var query = connection.query('SELECT * FROM compraproductos', [1], function(error, result){
            if(error){
               throw error;
            }else{
               var compras_productos = result;
               if(compras_productos.length > 0){
                  return res.status(200).send({ compras_productos });
               }else{
                  console.log('Registro no encontrado');
               }
            }
         }
        );

    },

    editarCompraProducto: function(req, res){
        var compra_productoId = req.params.id;
        var update = req.body;

        // CompraProducto.findByIdAndUpdate(compra_productoId, update, {new:true}, (err, compra_productoUpdated) => {
            
        //     if(err){
        //         if(!compra_productoUpdated) return res.status(404).send({message: 'No existe la compra del producto para actualizar'});
        //         return res.status(500).send({message: 'Error al actualizar'});
        //     } 

        //     return res.status(200).send({
        //         compra_producto: compra_productoUpdated 
        //     });

        // });

        var query = connection.query('UPDATE compraproductos SET compra=?, producto=?, cantidad=?, estado=? WHERE _id=?', [update.compra, update.producto, update.cantidad, update.estado, compra_productoId], function(error, result){
            if(error){
               throw error;
            }else{
               console.log("se actualizo");
               return res.status(200).send({ result });
            }
          }
         );

    },

    borrarCompraProducto: function(req, res){
        var compra_productoId = req.params.id;

        // CompraProducto.findByIdAndRemove(compra_productoId, (err, compra_productoRemoved) => {

        //     if(err){
        //         if(!compra_productoRemoved) return res.status(404).send({message: "No se puede eliminar esa compra del producto porque no se encuentra."});
        //         return res.status(500).send({message: 'No se ha podido borrar la compra del producto'});
        //     } 

        //     return res.status(200).send({
        //         compra_producto: compra_productoRemoved
        //     });
        // });

        var query = connection.query('DELETE FROM compraproductos WHERE _id=?', [compra_productoId], function(error, result){
            if(error){
               throw error;
            }else{
                console.log('Registro eliminado');
                return res.status(200).send({ result });
            }
         }
        );
    }
};

module.exports = controller;    