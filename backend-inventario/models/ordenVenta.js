var mongoose = require('mongoose');

let Schema = mongoose.Schema;
let autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);

mongoose.model('Empleado');
mongoose.model('Cliente');
mongoose.model('Producto');

var ordenVentaSchema = Schema({
    empleado: {
        type: Schema.ObjectId,
        ref: "Empleado",
        required: [true, 'El empleado es necesario']
    },
    cliente: {
        type: Schema.ObjectId,
        ref: "Cliente",
        required: [true, 'El cliente es necesario']
    },
    descripcion: String,
    fecha: {
        type: String,
        require: [true, 'La fecha es necesaria']
    },
    productos: [{
        type: Number,
        ref: "Producto",
        required: [true, 'El producto es necesario']
    }],
    total: {
        type: Number,
        required: [true, 'El total es necesario']
    }
});

ordenVentaSchema.plugin(autoIncrement.plugin, 'numero_venta');
var Shema = mongoose.model('numero_venta', ordenVentaSchema);
module.exports = mongoose.model('OrdenVenta', ordenVentaSchema);